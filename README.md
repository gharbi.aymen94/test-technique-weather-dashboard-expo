# Test technique - WeatherDashboard

Using the OpenWeatherMap API (http://www.openweathermap.com/current), a Weather Dashboard
App is built using ReactNative(Expo).
The App is composed of two pages:
- The “Home” page, displaying the list of the user’s weather widgets.
    - Each widget displays the name of the city, the current temperature in Celsius and an icon
    of the weather (http://erikflowers.github.io/weather-icons/)
- The “Settings” page, used to edit the content of the “Home” page.
    - The user can Add, Remove weather widgets.

Among the features included in the app:
- Retreiving real time data from Open Weather API
- Search for cities using Algolia engine
- State management with Redux
- Navigating with React Navigation
- Custom Font


### IOS Screenshots

![alt text](https://gitlab.com/gharbi.aymen94/test-technique-weather-dashboard-expo/-/raw/master/screenshots/ios/1.png?raw=true)

![alt text](https://gitlab.com/gharbi.aymen94/test-technique-weather-dashboard-expo/-/raw/master/screenshots/ios/2.png?raw=true)

![alt text](https://gitlab.com/gharbi.aymen94/test-technique-weather-dashboard-expo/-/raw/master/screenshots/ios/3.png?raw=true)

![alt text](https://gitlab.com/gharbi.aymen94/test-technique-weather-dashboard-expo/-/raw/master/screenshots/ios/4.png?raw=true)


### Android Screenshots

![alt text](https://gitlab.com/gharbi.aymen94/test-technique-weather-dashboard-expo/-/raw/master/screenshots/android/1.png?raw=true)

![alt text](https://gitlab.com/gharbi.aymen94/test-technique-weather-dashboard-expo/-/raw/master/screenshots/android/2.png?raw=true)

![alt text](https://gitlab.com/gharbi.aymen94/test-technique-weather-dashboard-expo/-/raw/master/screenshots/android/3.png?raw=true)

![alt text](https://gitlab.com/gharbi.aymen94/test-technique-weather-dashboard-expo/-/raw/master/screenshots/android/4.png?raw=true)
