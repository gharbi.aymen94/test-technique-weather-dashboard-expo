import {
    LOOKING_FOR_CITIES,
    LOOKING_FOR_CITIES_FAILED,
    NO_RESULT_FOUND,
    RESULTS_FOUND,
    CLEAR_RESULTS
} from "../actions/types";

const INITIAL_STATE = {
    loading: false,
    search: "",
    results: []    
};

export default (state = INITIAL_STATE, action) => {
    switch (action.type) {
        case LOOKING_FOR_CITIES:
            return {
                ...state,
                search: action.payload,
                loading: true
            };

        case LOOKING_FOR_CITIES_FAILED:
            return {
                ...state,
                loading: false,
                search: "",
                results: []
            };
    
        case RESULTS_FOUND:
            return {
                ...state,
                search: action.payload.query,
                results: action.payload.data
            };

        case NO_RESULT_FOUND:
            return {
                ...state,
                loading: false,
                search: action.payload,
                results: []
            };
        
        case CLEAR_RESULTS:
            return {
                ...state,
                results: []
            };

        default:
            return state;
    }
};
