import {
    ADD_PLACE_PENDING,
    ADD_PLACE_SUCCESS,
    ADD_PLACE_FAILURE,
    SWITCH_LEFT,
    SWITCH_RIGHT,
    REMOVE_PLACE_SUCCESS,
    REMOVE_PLACE_FAILURE,
    UPDATE_PLACE_SUCCESS,
    UPDATE_PLACE_FAILURE,
    UPDATE_PLACE_PENDING    
} from "../actions/types";

const INITIAL_STATE = {
    places: [
        {
            "dt": "1623520800",
            "lat": 48.86,
            "lon": 2.33,
            "name": "Paris",
            "temp": 15,
            "min": 10,
            "max": 20,
            "feels_like": 13,
            "pressure": 1011,
            "humidity": 89,
            "uvi": 7.41,            
            "visibility": 10000,
            "wind_speed": 0,
            "description": "Blue sky",
            "icon": "10d",
            "forecast": [
                {
                    "dt": 1623520800,
                    "weather_icon": "10d",
                    "min": 12,
                    "max": 15
                },
                {
                    "dt": 1623520800,
                    "weather_icon": "10d",
                    "min": 10,
                    "max": 17
                },
                {
                    "dt": 1623520800,
                    "weather_icon": "10d",
                    "min": 11,
                    "max": 15
                }
            ]
        }
    ],
    selected: 0,
    loading: false
};

export default (state = INITIAL_STATE, action) => {
    switch (action.type) {
        // Pending
        case ADD_PLACE_PENDING:
        case UPDATE_PLACE_PENDING:
            return { ...state, loading: true };

        // Success Add  
        case ADD_PLACE_SUCCESS:
            return {
                ...state,
                loading: false,
                places: [
                    ...state.places,
                    action.payload
                ]
            };

        case UPDATE_PLACE_SUCCESS:
            return {
                ...state,
                loading: false,
                places: [
                    ...[...state.places].slice(0, action.payload.selected),
                    action.payload.obj,
                    ...[...state.places].slice(action.payload.selected + 1, state.places.length)
                ]
            };
            
        // Failure Add/Update
        case ADD_PLACE_FAILURE:
        case UPDATE_PLACE_FAILURE:
            return {
                ...state,
                loading: false
            };
        
        // Switch cases
        case SWITCH_RIGHT:            
            return {
                ...state,
                selected: action.payload
            };
        case SWITCH_LEFT:
            return {
                ...state,
                selected: action.payload
            };

        // Remove place
        case REMOVE_PLACE_SUCCESS:
            return {
                ...state,
                places: action.payload.places,
                selected: action.payload.selected
            };        
        case REMOVE_PLACE_FAILURE:            
        
        default:
            return state;
    }
};
