import { combineReducers } from "redux";
import HomeReducer from "./HomeReducer"
import SettingsReducer from "./SettingsReducer"

export default combineReducers({
    home: HomeReducer,
    settings: SettingsReducer
});
