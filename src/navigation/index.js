import * as React from 'react';
import { NavigationContainer } from '@react-navigation/native';
import { createStackNavigator } from "@react-navigation/stack"
import { navigationRef } from './service';
import { HomeScreen, SettingsScreen } from "../screens"

const Tab = createStackNavigator();


const NavigationProvider = () => {
  return (
    <NavigationContainer ref={navigationRef}>
      <Tab.Navigator 
        screenOptions={{
          headerShown: false
        }}
      >
        <Tab.Screen name="Home" component={HomeScreen} />
        <Tab.Screen name="Settings" component={SettingsScreen} />
      </Tab.Navigator>
    </NavigationContainer>
  );
}

export default NavigationProvider;