export const convertKtoC = (temp_F) => Math.floor(temp_F - 273,15)

export const capitalize = (name) => name.charAt(0).toUpperCase() + name.slice(1)

export const convertDtToDate = (UNIX_timestamp) => {
    let a = new Date(UNIX_timestamp * 1000);
    let months = ['January','February','Mars','April','May','June','July','August','September','October','November','December'];
    let days = ["Sunday", "Monday", "Tuesday", "Wednesday", "Thursday", "Friday", "Saturday"];
    let year = a.getFullYear();
    let month = months[a.getMonth()];
    let date = a.getDate();
    let day = a.getDay()
    let time = days[day] + ', ' + date + ' ' + month + ' ' + year;
    return time;
}