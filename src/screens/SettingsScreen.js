import React, { Component } from "react"
import { View, Text, FlatList, TouchableOpacity, Image } from "react-native";
import { SearchBar, ResultItem } from "@components"
import { connect } from "react-redux";
import { searchCity } from "@actions"

class SettingsScreen extends Component {
    onPress = (word) => {
        this.props.searchCity(word)
    }

    goBack = () => {
        this.props.navigation.goBack()
    }

    render () {
        const { settings } = this.props;

        return (
            <View style={styles.container}>
                <View style={styles.header}>                           
                    <TouchableOpacity style={styles.backBtnContainer} onPress={this.goBack}>
                        <Image
                            style={styles.backBtn}
                            source={require("../assets/images/left.png")}
                        />
                    </TouchableOpacity>
                    <Text style={styles.title}>City Search</Text>  
                </View>                       
                <SearchBar onPress={this.onPress} />
                <View style={{ opacity: (settings.loading) ? 1 : 0 }}>
                    <Text style={styles.notice}>Click on the city you want to add to your dashboard.</Text>
                    <FlatList 
                        data={settings.results}
                        renderItem={({ item }) => (
                            <ResultItem 
                                name={item.name} 
                                lat={item.lat} 
                                lon={item.lon} 
                                country_code={item.country_code}
                                afterPress={this.goBack} 
                            />
                        )}
                        keyExtractor={(item, idx) => idx.toString()}
                    />             
                </View>    
            </View>
        );
    }
}

const styles = {
    container: {
        backgroundColor: "white",
        alignItems: "center",
        flex: 1,
        padding: 20,
        paddingTop: 40
    },
    header: {
        flexDirection: "row",
        justifyContent: "space-between",
        width: "100%",
    },
    title:{
        fontFamily: "bold",
        fontSize: 30
    },
    backBtnContainer: {
        height: 40,
        width: 40,
        justifyContent: "center",
        alignItems: "center",
        alignSelf: "flex-start",
        marginBottom: 20
    },
    backBtn: {
        height: 30,
        width: 30
    },
    notice: {
        fontFamily: "regular",
        fontSize: 14,
        color: "grey",
        paddingVertical: 20
    },
    resultsContainer: {
        flex: 1,
        width: "100%",
        marginVertical: 40,
        backgroundColor: "white",
        flexDirection: "row"
    },
    resultsListContainer: {
        flex: 2,
        borderLeftWidth: 0.5,
        borderColor: "grey"
    }
}

const mapStateToProps = state => {
    return {
        settings: state.settings,
    };
};

export default connect(
    mapStateToProps,
    { searchCity }
)(SettingsScreen);
