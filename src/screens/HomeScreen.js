import React, { Component } from "react"
import { View, FlatList, ActivityIndicator, Text, ImageBackground, Platform } from "react-native";
import { Place, PlaceIndicator, Dashboard, ForecastItem, WidgetButton, DashboardItem, PlaceWeatherDesc } from "@components"  
import { switchPlace, removePlace, updatePlace } from "@actions"
import { connect } from "react-redux";
import { SWITCH_LEFT, SWITCH_RIGHT } from "@actions/types"
import { m_yellow, m_grey, vw, vh  } from "@config"
import { convertDtToDate, capitalize } from "@helpers"


class HomeScreen extends Component {    
    onLeftPress = (selected, len) => {
        const newSelected = (selected == 0) ? (len-1) : (selected-1)
        this.props.switchPlace(SWITCH_LEFT, newSelected)
    }

    onRightPress = (selected, len) => {
        const newSelected = (selected == (len-1)) ? 0 : (selected+1)
        this.props.switchPlace(SWITCH_RIGHT, newSelected)
    }

    render () {
        const { home, navigation, removePlace, updatePlace } = this.props

        return (
            <View style={styles.container}>
                <ActivityIndicator animating={home.loading} size="large" color={m_yellow} style={styles.loader} />                                                            
                <View style={styles.container2}>
                    <View style={styles.tmpContainer}>
                        <DashboardItem text="Today" value={home.places[home.selected].temp} unit="°C" />
                        <View style={{ height: 20 }} />
                        <DashboardItem text="Feels like" value={home.places[home.selected].feels_like} unit="°C" />
                    </View>
                    <View style={styles.todayContainer}> 
                        <View style={{ flex: 1 }}>
                            <Text style={styles.todayText}>{convertDtToDate(home.places[home.selected].dt)}</Text>
                            <Place name={home.places[home.selected].name} />
                            <PlaceWeatherDesc 
                                text={capitalize(home.places[home.selected].description)}
                                uri={"http://openweathermap.org/img/w/"+home.places[home.selected].icon+".png"} 
                            />
                            <PlaceIndicator
                                size={home.places.length} selected={home.selected} 
                                onLeftPress={() => this.onLeftPress(home.selected, home.places.length)} 
                                onRightPress={() => this.onRightPress(home.selected, home.places.length)} 
                            />
                        </View>
                        <View style={styles.widgetContainer}>
                            <WidgetButton img="add.png" onPress={() => navigation.navigate("Settings")} />
                            <WidgetButton img="refresh.png" onPress={() => updatePlace(home.selected, home.places)} />
                            <WidgetButton img="rubbish.png" onPress={() => removePlace(home.selected, home.places)} />
                        </View>
                    </View>                                    
                </View>
                <Dashboard info={home} />
                <Text style={styles.forecastText}>Prevision for the upcoming 7 days</Text>                
                <View style={{ marginHorizontal: 40 }}>
                    <FlatList 
                        data={home.places[home.selected].forecast}
                        renderItem={({ item }) => (<ForecastItem dt={item.dt} icon={item.weather_icon} min={item.min} max={item.max} />)}
                        keyExtractor={(item, idx) => idx.toString()}    
                        horizontal
                        ItemSeparatorComponent={() => (<View style={styles.separator} />)}
                        style={{ paddingBottom: 10 }}               
                    />
                </View>
                <ImageBackground style={styles.bg} source={require("../assets/images/bg.jpg")}/>
            </View>
        );
    }
}

const styles = {
    loader: {
        position: "absolute",
        top: 10,
        left: vw * 48
    },
    bg: {
        position: "absolute",        
        width: vw*100,
        height: vh*38,
        zIndex: -1000,
        opacity: 0.2
    },
    separator: {
        height: "100%",
        width: 10,
        color: m_grey
    },
    container: {
        backgroundColor: "white",
        flex: 1,
        paddingVertical: 40
    },
    container2: {
        alignItems: "center",
        flex: 1,
        paddingHorizontal: 34,
        flexDirection: "row",
        marginTop: Platform.OS == "ios" ? 40 : 20       
    },
    forecastText: {
        fontFamily: "bold",
        fontSize: 18,
        paddingBottom: 3,
        marginHorizontal: 40
    },
    tmpContainer: {
        flex: 1,
        width: "35%",
        height: "100%",
        paddingTop: 20
    },
    todayContainer: {
        width: "65%",
        height: "100%",
        paddingTop: 20,
        flexDirection: "row"
    },
    todayText: {
        fontSize: 15,
        fontFamily: "regular",
        paddingLeft: 10
    },
    widgetContainer: {
        justifyContent: "center",
        paddingBottom: 20
    }
}

const mapStateToProps = state => {
    return {
        home: state.home,
    };
};

export default connect(
    mapStateToProps,
    { switchPlace, removePlace, updatePlace }
)(HomeScreen);
