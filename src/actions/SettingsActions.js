import { algoliaClient } from "@config"
import { 
    LOOKING_FOR_CITIES,
    LOOKING_FOR_CITIES_FAILED,
    RESULTS_FOUND,
    NO_RESULT_FOUND
} from "./types"

export const searchCity = (word) => {
    return dispatch => {
        dispatch({
            type: LOOKING_FOR_CITIES,
            payload: word
        })

        algoliaClient.request({            
            method: "post",
            params: {
                type: "city"
            },
            data: {
                "query": word
            }
        })
            .then(res => {
                
                let searchResults = []
                if (res.data.hits.length == 0) {
                    return dispatch({
                        type: NO_RESULT_FOUND,
                        payload: word
                    })
                }
                res.data.hits.map((val, idx) => {
                    if (val.is_city) {
                        searchResults.push({
                            name: val.locale_names.default[0],
                            lat: val._geoloc.lat,
                            lon: val._geoloc.lng,
                            country_code: val.country_code,
                        })
                    }
                })

                console.log(RESULTS_FOUND, searchResults)

                dispatch({
                    type: RESULTS_FOUND,
                    payload: {
                        query: word,
                        data: searchResults
                    }
                })                
            })
            .catch(error => {
                console.log(LOOKING_FOR_CITIES_FAILED, error)

                dispatch({
                    type: LOOKING_FOR_CITIES_FAILED
                })
            })
    }
}