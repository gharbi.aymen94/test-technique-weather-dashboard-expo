import { OW_APPKEY, openWeatherClient } from "@config"
import { 
    ADD_PLACE_PENDING,
    ADD_PLACE_SUCCESS,
    ADD_PLACE_FAILURE, 
    REMOVE_PLACE_SUCCESS,
    REMOVE_PLACE_FAILURE,
    UPDATE_PLACE_PENDING,
    UPDATE_PLACE_SUCCESS,
    UPDATE_PLACE_FAILURE,
    CLEAR_RESULTS
} from "./types"
import { convertKtoC } from "@helpers"

//addPlace(43.6, 1.44, "Toulouse, FR")
export const addPlace = (lat, lon, name, callb) => {
    return (dispatch) => {
        dispatch({ type: ADD_PLACE_PENDING })
        openWeatherClient.request({
            params: {
                lat, 
                lon, 
                "exclude": "hourly, minutely", 
                "appid": OW_APPKEY
            },
            method: "get"
        }).then(function (response) {
            // handle success
            let obj = {
                "dt": response.data.current.dt,
                "lat": response.data.lat,
                "lon": response.data.lon,
                "name": name,
                "temp": convertKtoC(response.data.current.temp),
                "min": convertKtoC(response.data.daily[0].temp.min),
                "max": convertKtoC(response.data.daily[0].temp.max),
                "feels_like": convertKtoC(response.data.current.feels_like),
                "pressure": response.data.current.pressure,
                "humidity": response.data.current.humidity,
                "uvi": response.data.current.uvi,            
                "visibility": response.data.current.visibility,
                "wind_speed": response.data.current.wind_speed,
                "description": response.data.current.weather[0].description,
                "icon": response.data.current.weather[0].icon,
                "forecast": []
            }

            response.data.daily.map((val, idx) => {
                if (idx > 0) {
                    obj["forecast"].push({
                        "dt": val.dt,
                        "weather_icon": val.weather[0].icon,
                        "min": convertKtoC(val.temp.min),
                        "max": convertKtoC(val.temp.max)
                    })
                }
            })

            //console.log(obj)            
            
            // Dispatching success only after successful creation of the object            
            dispatch({
                type: ADD_PLACE_SUCCESS,
                payload: obj
            })
            dispatch({
                type: CLEAR_RESULTS
            })        
        }).catch(error => {
            // handle fetching error
            console.log(error)
            dispatch({
                type: ADD_PLACE_FAILURE
            })
        })
        callb();
    }
}

export const removePlace = (selected, oldPlaces) => {
    if (oldPlaces.length == 1) {
        return ({
            type: REMOVE_PLACE_FAILURE
        })
    } 

    const newPlaces = oldPlaces.filter((val, idx) => {
        return (idx != selected)
    })

    if (newPlaces.length == (oldPlaces.length - 1)) {
        return ({
            type: REMOVE_PLACE_SUCCESS,
            payload: {
                places: newPlaces,
                selected: (selected == 0) ? (newPlaces.length-1) : (selected-1)
            }
        })
    } else {
        return ({
            type: REMOVE_PLACE_FAILURE
        })
    }
}

export const updatePlace = (selected, oldPlaces) => {
    return dispatch => {
        const { lat, lon, name } = oldPlaces[selected]
        dispatch({ type: UPDATE_PLACE_PENDING })
        openWeatherClient.request({
            params: {
                lat, 
                lon, 
                "exclude": "hourly, minutely", 
                "appid": OW_APPKEY
            },
            method: "get"
        }).then(function (response) {
            // handle success
            let obj = {
                "dt": response.data.current.dt,
                "lat": response.data.lat,
                "lon": response.data.lon,
                "name": name,
                "temp": convertKtoC(response.data.current.temp),
                "min": convertKtoC(response.data.daily[0].temp.min),
                "max": convertKtoC(response.data.daily[0].temp.max),
                "feels_like": convertKtoC(response.data.current.feels_like),
                "pressure": response.data.current.pressure,
                "humidity": response.data.current.humidity,
                "uvi": response.data.current.uvi,            
                "visibility": response.data.current.visibility,
                "wind_speed": response.data.current.wind_speed,
                "description": response.data.current.weather[0].description,
                "icon": response.data.current.weather[0].icon,
                "forecast": []
            }

            response.data.daily.map((val, idx) => {
                if (idx > 0) {
                    obj["forecast"].push({
                        "dt": val.dt,
                        "weather_icon": val.weather[0].icon,
                        "min": convertKtoC(val.temp.min),
                        "max": convertKtoC(val.temp.max)
                    })
                }
            })                        
            // Dispatching success only after successful creation of the object            
            dispatch({
                type: UPDATE_PLACE_SUCCESS,
                payload: {
                    obj,
                    selected
                }
            })        
        }).catch(error => {
            // handle fetching error
            console.log(UPDATE_PLACE_FAILURE, error)
            dispatch({
                type: UPDATE_PLACE_FAILURE
            })
        })
    }        
}

export const switchPlace = (type, newSelected) => ({
    type,
    payload: newSelected
})


//https://api.openweathermap.org/data/2.5/onecall?lat=33.44&lon=-94.04&exclude=hourly,minutely&appid=86327ae3b0fefb077081029bd2fd6ade
