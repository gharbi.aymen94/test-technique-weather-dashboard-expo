import React, { Component } from "react"
import { View, Text } from "react-native";
import DashboardItem from "./DashboardItem"
import { m_grey } from "@config"

export default class Dashboard extends Component {    
    render () {
        const { info } = this.props
        
        return (
            <View style={styles.container}>
                <View style={{ paddingHorizontal: 40 }}>                    
                    <Text style={styles.dashboardText}>
                        Dashboard
                    </Text>
                </View>
                
                <View style={styles.dashboardRow}>
                    <DashboardItem text="UVI" value={info.places[info.selected].uvi} />
                    <DashboardItem text="Wind" value={info.places[info.selected].wind_speed} unit="km/h" />
                    <DashboardItem text="Min" value={info.places[info.selected].min} unit="°C" />
                </View>
                <View style={styles.dashboardRow}>
                    <DashboardItem text="Humidity" value={info.places[info.selected].humidity} unit="%" />
                    <DashboardItem text="Pressure" value={info.places[info.selected].pressure} unit="hPa" />
                    <DashboardItem text="Max" value={info.places[info.selected].max} unit="°C" />
                </View>
            </View>
        );
    }
}

const styles = {
    container: {
        paddingVertical: 10,
        alignSelf: "flex-start", 
        width: "100%"
    },    
    line2: { flexDirection: "row", justifyContent: "space-between" },
    todayText: { fontSize: 18, fontFamily: "bold", maxHeight: 40 },
    todayTemp: { fontSize: 28, fontFamily: "bold" },
    weatherImg: {
        height: 20,
        width: 20,
    },
    date: { marginLeft: 10, fontFamily: "regular", fontSize: 17 },
    minTemp: { fontFamily: "regular", color: "red" },
    maxTemp: { fontFamily: "bold", color: "blue", marginLeft: 10 },
    limiter: { height: 2, width: "100%", backgroundColor: "blue", marginBottom: 10, marginTop: 25, borderRadius: 2},
    dashboardRow: { flexDirection: "row", width: "100%", justifyContent: "space-evenly", paddingVertical: 10, backgroundColor: m_grey },
    dashboardText: {
        fontFamily: "bold",
        fontSize: 18
    }
}
