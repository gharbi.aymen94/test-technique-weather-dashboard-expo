import React, { Component } from "react"
import { View, Text, TouchableOpacity } from "react-native";
import { connect } from "react-redux";
import { addPlace } from "@actions"

class ResultItem extends Component {    
    onPress = () => {
        const { name, lat, lon, addPlace, afterPress } = this.props;
        addPlace(lat, lon, name, afterPress);
    }

    render () {         
        return (
            <TouchableOpacity style={styles.container} onPress={this.onPress}>
                <Text style={styles.name}>{this.props.name}, {this.props.country_code.toUpperCase()}</Text>
            </TouchableOpacity>            
        );
    }
}

const styles = {
    container: {
        backgroundColor: "#F6F6F6",
        height: 100,
        padding: 22,
        flexDirection: "row",
        borderWidth: 1,
        borderRadius: 10,
        marginBottom: 20,
        alignItems: "center",
        justifyContent: "center"
    },
    name: {
        fontFamily: "regular",
        fontSize: 18,
        textAlign: "center"
    }
}

export default connect(
    null,
    { addPlace }
)(ResultItem);
