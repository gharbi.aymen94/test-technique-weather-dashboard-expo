import React, { Component } from "react"
import { View, Text, Image } from "react-native";
import { convertDtToDate } from "@helpers"
import { m_yellow, m_grey, m_blue } from "@config"

export default class ForecastItem extends Component {    
    render () {         
        return (
            <View style={styles.container}>
                <Text style={styles.day}>{convertDtToDate(this.props.dt).split(",")[0]}</Text>
                <View style={styles.subContainer}>
                    <View style={{ paddingRight: 20 }}>
                        <Text style={styles.min}>{this.props.min} <Text style={{ fontSize: 14 }}>°C</Text></Text>
                        <Text style={styles.max}>{this.props.max} <Text style={{ fontSize: 14 }}>°C</Text></Text>
                    </View>
                    <Image
                        style={styles.weatherIcon}
                        source={{ uri: "http://openweathermap.org/img/w/"+this.props.icon+".png" }}
                    />                
                </View>                
            </View>            
        );
    }
}

const styles = {
    container: {
        height: 100,
        width: 100,
        backgroundColor: m_yellow,
        padding: 12,
        borderRadius: 20,
    },
    day: {
        fontFamily: "bold",
        fontSize: 13,
        flex: 2,
        textAlign: "center"
    },
    weatherIcon: {
        height: 20,
        width: 20,
        flex: 1,
        alignSelf: "center"
    },
    min: {
        color: "black",
        flex: 1,
        fontSize: 16,
        fontFamily: "regular",
        textAlign: "center"
    },
    max: {
        color: "black",
        flex: 1,
        fontSize: 16,
        fontFamily: "regular",
        textAlign: "center"
    },
    subContainer: {
        flexDirection: "row",
        justifyContent: "space-between",
        flex: 2
    }
}