import React, { Component } from "react"
import { View, Text } from "react-native";
import { m_blue, m_grey, m_yellow } from "@config"

export default class DashboardItem extends Component {    
    render () {         
        return (
            <View style={styles.container}>
                <Text style={styles.dashboardText}>{this.props.text}</Text>
                <Text style={styles.dashboardValue}>{this.props.value}</Text>
                <Text style={{ fontFamily: "regular", color: m_grey }}>{this.props.unit}</Text>
            </View>            
        );
    }
}

const styles = {
    container: {
        backgroundColor: m_blue,
        width: 90,
        height: 90,
        justifyContent: "center",
        alignItems: "center",
        borderRadius: 10,
        // Shadow
        shadowColor: "#000",
        shadowOffset: {
            width: 0,
            height: 3
        },
        shadowOpacity: 0.3,
        shadowRadius: 4.0,

        elevation: 7
    },
    dashboardText: { flex: 2, fontSize: 15, fontFamily: "bold", color: m_grey },
    dashboardValue: { flex: 3, fontSize: 30, fontFamily: "bold", color: m_yellow },
}