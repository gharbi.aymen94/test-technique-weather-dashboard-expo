import React, { Component } from "react"
import { View, Text, Image } from "react-native";

export default class PlaceWeatherDesc extends Component {    
    render () {         
        return (
            <View style={styles.container}>                
                <Image 
                    source={{ uri: this.props.uri }}
                    style={styles.icon}
                />
                <Text style={styles.desc}>{this.props.text}</Text>
            </View>            
        );
    }
}

const styles = {
    container: {
        marginBottom: 20,
        paddingLeft: 18,
        flexDirection: "row",
    },
    icon: {
        height: 40,
        width: 40
    },
    desc: {
        paddingTop: 10,
        paddingLeft: 10,
        fontFamily: "regular",
        fontSize: 15
    }
}