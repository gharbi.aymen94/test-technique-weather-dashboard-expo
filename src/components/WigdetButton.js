import React, { Component } from "react"
import { TouchableOpacity, Image } from "react-native";
import { m_yellow } from "@config"

export default class WidgetButton extends Component {    
    render () {         
        return (
            <TouchableOpacity 
                style={styles.widgetBtn} 
                onPress={this.props.onPress}
            > 
                <Image 
                    source={(this.props.img == "add.png") ? 
                        require('../assets/images/add.png') : (this.props.img == "refresh.png") ?  
                        require('../assets/images/refresh.png') :
                        require('../assets/images/delete.png')
                    }
                    style={styles.widgetImg}
                />                        
            </TouchableOpacity>            
        );
    }
}

const styles = {
    widgetBtn: {
        width: 35,
        height: 30,
        alignItems: "center",
        justifyContent: "center",
        backgroundColor: m_yellow,
        marginTop: 10,
        borderBottomLeftRadius: 5,
        borderTopLeftRadius: 5,
        // Shadow
        shadowColor: "#000",
        shadowOffset: {
            width: 0,
            height: 3
        },
        shadowOpacity: 0.3,
        shadowRadius: 4.0,

        elevation: 12
    },
    widgetImg: {
        height: 17,
        width: 17
    }
}