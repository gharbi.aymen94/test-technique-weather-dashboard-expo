import React, { Component } from "react"
import { View, FlatList, TouchableOpacity, Image } from "react-native";
import { m_blue2 } from "@config"

export default class PlaceIndicator extends Component {    
    render () {         
        return (
            <View style={styles.container}>                
                <FlatList
                    data={new Array(this.props.size)}
                    renderItem={({ item, index }) => (<View style={this.props.selected == index ? styles.selected : styles.unselected } />)}
                    keyExtractor={(item, idx) => idx.toString()}
                    horizontal={true}
                    style={{ maxWidth: "70%" }}
                />
                <View style={styles.indicator}>
                    <TouchableOpacity style={styles.arrowContainer} onPress={this.props.onLeftPress}>
                        <Image 
                            source={require('../assets/images/left-arrow.png')}
                            style={styles.settingsImg}
                        />
                    </TouchableOpacity>    
                    <TouchableOpacity style={styles.arrowContainer} onPress={this.props.onRightPress}>
                        <Image 
                            source={require('../assets/images/right-arrow.png')}
                            style={styles.settingsImg}
                        />
                    </TouchableOpacity>
                </View>                                                
            </View>            
        );
    }
}

const styles = {
    container: {
        alignItems: "center"
    },
    selected: {
        backgroundColor: "black", 
        width: 6, 
        height: 6, 
        borderRadius: 3,
        marginHorizontal: 2
    },
    unselected: { 
        backgroundColor: m_blue2, 
        width: 6, 
        height: 6, 
        borderRadius: 3,
        marginHorizontal: 2 
    },
    indicator: {
        flexDirection: "row",
        justifyContent: "center"
    },
    arrowContainer: { 
        height: 36, 
        width: 50,  
        alignItems: "center", 
        justifyContent: "center",
        backgoundColor: "yellow",
    },
    settingsImg: {
        height: 15,
        width: 15,
    }
}