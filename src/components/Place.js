import React, { Component } from "react"
import { Text, View } from "react-native";

export default class Place extends Component {    
    render () {
        return (
            <View style={styles.containerPlace}>
                <Text style={styles.placeName}>{this.props.name}</Text>
            </View>
        );
    }
}

const styles = { 
    containerPlace: {
        width: "100%",
        paddingHorizontal: 10,
        opacity: 0.9,
        maxHeight: "25%"
    },
    placeName: { 
        fontSize: 28, 
        fontFamily: "bold", 
    }
}