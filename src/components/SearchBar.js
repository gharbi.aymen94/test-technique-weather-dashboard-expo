import React, { Component } from "react"
import { View, TextInput, TouchableOpacity, Image } from "react-native";

export default class SearchBar extends Component {    
    state = {
        search: ""
    }

    render () {         
        return (
            <View style={styles.container}>
                <TextInput 
                    style={styles.searchInput} 
                    placeholder="Find your city ..."
                    onChangeText={search => this.setState({ search })}
                    value={this.state.search}  
                />                
                <TouchableOpacity style={styles.searchBtnContainer} onPress={() => this.props.onPress(this.state.search)}>
                    <Image
                        style={styles.searchBtn}
                        source={require("../assets/images/search.png")}
                    />
                </TouchableOpacity>
            </View>            
        );
    }
}

const styles = {
    container: {
        height: 50,
        flexDirection: "row",
        borderBottomWidth: 1,
        borderColor: "black",
    },
    searchInput: {
        flex: 3,
        fontFamily: "regular",
        fontSize: 18,
        paddingLeft: 10
    },
    searchBtnContainer: {
        flex: 1,
        justifyContent: "center",
        alignItems: "space-between"
    },
    searchBtnText: {
        fontFamily: "bold",
        fontSize: 18,
        color: "black",
        paddingHorizontal: 4
    },
    searchBtn: {
        height: 23,
        width: 23,
    }
}