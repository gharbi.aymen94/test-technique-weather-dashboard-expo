// Utils
import { Dimensions, Platform } from "react-native";

// Relative to the phone using the app
const { height, width } = Dimensions.get("window");

export const vh = height / 100;
export const vw = width / 100;
export const vmin = Math.min(vh, vw);
export const vmax = Math.max(vh, vw);