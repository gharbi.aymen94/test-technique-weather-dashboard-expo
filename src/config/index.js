import axios from "axios";

export * from "./colors"
export * from "./dimensions"

// HTTP Client
export const openWeatherClient = axios.create({
    baseURL: `https://api.openweathermap.org/data/2.5/onecall`,
    timeout: 7000,
    headers: {
        "Content-Type": "application/json"
    }
});

export const algoliaClient = axios.create({
    baseURL: `https://places-dsn.algolia.net/1/places/query`,
    timeout: 7000,
    headers: {
        "Content-Type": "application/json"
    }
});

// Constants
export const OW_APPKEY = "86327ae3b0fefb077081029bd2fd6ade" 



