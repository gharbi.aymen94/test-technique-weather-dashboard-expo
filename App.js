import React, { Component } from "react";
import { Platform, View, StatusBar } from "react-native";
import "react-native-gesture-handler"
import * as Font from 'expo-font';

// Utils
import { Provider } from "react-redux";
import NavigationProvider from "@navigation";
import store from "@store";


export default class App extends Component {
    state = {
        fontsLoaded: false,
    };

    async loadFonts() {
        await Font.loadAsync({
            regular: require('./src/assets/fonts/BalooTammudu2-Regular.ttf'),
            bold: require('./src/assets/fonts/BalooTammudu2-Bold.ttf'),
        });

        this.setState({ fontsLoaded: true });
    }

    componentDidMount() {
        this.loadFonts();
    }

    render() {
        if (this.state.fontsLoaded) {
            return (
                <Provider store={store}>
                    <StatusBar
                        backgroundColor="black"
                        barStyle={
                            Platform.OS === "ios" ? "dark-content" : "light-content"
                        }
                    />
                    <View style={{ flex: 1 }}>
                        <NavigationProvider />
                    </View>
                </Provider>
            );
        } else {
            return null;
        }
    }
}


